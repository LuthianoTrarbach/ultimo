package ${package}.config.broker;

import org.springframework.cloud.stream.annotation.Input;
import org.springframework.messaging.SubscribableChannel;

public interface BrokerInput {
	
	String INPUT_BROKER = "exchangeInput";
	
	@Input(BrokerInput.INPUT_BROKER)
	SubscribableChannel inputBroker();
}

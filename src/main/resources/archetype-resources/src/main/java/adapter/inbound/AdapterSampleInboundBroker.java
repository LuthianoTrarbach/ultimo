package  ${package}.adapter.inbound;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.cloud.stream.annotation.EnableBinding;
import org.springframework.cloud.stream.annotation.StreamListener;
import org.springframework.web.client.RestTemplate;

import  ${package}.config.broker.BrokerInput;
import  ${package}.domain.Sample;

@EnableBinding(BrokerInput.class)
public class AdapterSampleInboundBroker {

	@Value("endereco no yml")
	private String url;

	private RestTemplate restTemplate;

	public AdapterSampleInboundBroker() {
		 restTemplate = new RestTemplate();
	}

	@StreamListener(target = BrokerInput.INPUT_BROKER)
	public void receiverMessage(String msg) {

		Sample sample = new Sample();
		sample.setId(msg);

		url = url + "/ordem";

		// Faz a chamada REST à URL montada
		restTemplate.getForEntity(url, Sample.class);

	}

}
